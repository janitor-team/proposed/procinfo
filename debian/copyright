Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: procinfo NG
Source: https://sourceforge.net/projects/procinfo-ng/

Files: *
Copyright: 2007-2009 Thaddeus Messenger <tabrisnet@users.sourceforge.net,
                                         tabris@tabris.net>
License: GPL-2

Files: lib/prettyPrint.cpp
       lib/routines.cpp
       lib/timeRoutines.cpp
Copyright: 2007-2009 Thaddeus Messenger <tabrisnet@users.sourceforge.net,
                                         tabris@tabris.net>
License: LGPL-2.1

Files: debian/*
Copyright: 1997      Christoph Lameter <clameter@debian.org>
           1997-1999 Michael Alan Dorman <mdorman@debian.org>
           2000-2001 Lonnie Sauter <sauter@debian.org>
           2002      Colin Watson <cjwatson@debian.org>
           2005      Florian Ernst <florian@debian.org>
           2007      Antonio José Calderón <neo@neocalderon.net>
           2008-2009 Giuseppe Iuculano <giuseppe@iuculano.it>
           2017      Joao Eriberto Mota Filho <eriberto@debian.org>
           2017      Mattia Rizzolo <mattia@debian.org>
           2020      Joao Paulo Lima de Oliveira <jlima.oliveira11@gmail.com>
License: GPL-2+

License: GPL-2 or GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2.1
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU Lesser General
 Public License can be found in "/usr/share/common-licenses/LGPL-2-1".
